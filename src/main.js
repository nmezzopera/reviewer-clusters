import Vue from "vue";
import App from "./App.vue";
import { createProvider } from "./vue-apollo";
// import stats from "./data/stats.json";
Vue.config.productionTip = false;

// const frontenders = [];

// for (const p of stats.people) {
//   for (const t of p.user.type) {
//     console.log(t);
//     if (t.r.includes("frontend")) {
//       frontenders.push(p.key);
//       break;
//     }
//   }
// }

// console.log(frontenders);

new Vue({
  apolloProvider: createProvider(),
  render: (h) => h(App),
}).$mount("#app");
